import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { withStyles } from '@material-ui/core/styles';
import InfiniteScroll from 'react-infinite-scroller';
import CircularProgress from '@material-ui/core/CircularProgress';
import { connect } from 'react-redux';

import { fetchGiphyAction } from '../actions/apiActions';
import {
  getOffset,
  getTotalCount,
  getSearch,
} from '../reducers/mainReducer';

const styles = theme => ({
  infiniteScrollContainer: {
    flex: '1 1 auto',
    overflowX: 'auto',
  },
});

const mapStateToProps = state => ({
  offset: getOffset(state),
  totalCount: getTotalCount(state),
  search: getSearch(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchGiphys: fetchGiphyAction,
}, dispatch);

class InfiniteScrollContainer extends React.Component {
    fetchMoreGiphys = () => {
      const {
        fetchGiphys, batchSize, search, offset,
      } = this.props;
      fetchGiphys(search, batchSize, offset + batchSize);
    };

    render() {
      const {
        classes, children, totalCount, offset, batchSize,
      } = this.props;
      return (
        <div className={classes.infiniteScrollContainer} ref={ref => this.scrollParentRef = ref}>
          <InfiniteScroll
            hasMore={offset + batchSize < totalCount}
            loadMore={this.fetchMoreGiphys}
            loader={<CircularProgress key={1} />}
            useWindow={false}
            getScrollParent={() => this.scrollParentRef}
          >
            {children}
          </InfiniteScroll>
        </div>
      );
    }
}

InfiniteScrollContainer.propTypes = {
  fetchGiphys: PropTypes.func.isRequired,
};

const StyledScroll = withStyles(styles)(InfiniteScrollContainer);
export default connect(mapStateToProps, mapDispatchToProps)(StyledScroll);
