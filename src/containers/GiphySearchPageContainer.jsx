import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CircularProgress from '@material-ui/core/CircularProgress';

import InfiniteScroll from './InfiniteScroll';
import { fetchGiphyAction } from '../actions/apiActions';
import { addFavoriteAction, removeFavoriteAction } from '../actions/localStorageActions';
import {
  getGiphys, getIsFetching, getOffset, getTotalCount, getSearch,
} from '../reducers/mainReducer';

import { getFavoritesLength } from '../selectors/selectors';

import GiphyCard from '../components/GiphyCard';
import GiphySearch from '../components/GiphySearch';

const GIPHY_BATCH_SIZE = 50;
const DEFAULT_GIPHY_SEARCH = 'popular';

const mapStateToProps = state => ({
  giphys: getGiphys(state),
  offset: getOffset(state),
  totalCount: getTotalCount(state),
  isFetching: getIsFetching(state),
  numberOfFavorites: getFavoritesLength(state),
  search: getSearch(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchGiphys: fetchGiphyAction,
  addFavorite: addFavoriteAction,
  removeFavorite: removeFavoriteAction,
}, dispatch);

class GiphySearchPageContainer extends Component {
  static renderLoading() {
    return <div><CircularProgress /></div>;
  }

  componentWillMount() {
    const { search, giphys, fetchGiphys } = this.props;
    if (!search && !giphys) {
      fetchGiphys(DEFAULT_GIPHY_SEARCH, GIPHY_BATCH_SIZE, 0);
    }
  }

    fetchGiphys = (search) => {
      const { fetchGiphys } = this.props;
      fetchGiphys(search, GIPHY_BATCH_SIZE, 0);
    };

    clickFavorite = ({ isFavorite, id }, index) => {
      const { addFavorite, removeFavorite } = this.props;
      if (isFavorite) {
        removeFavorite(id, index);
      } else {
        addFavorite(id, index);
      }
    };

    renderGiphys() {
      const {
        giphys,
      } = this.props;
      if (!giphys.length) return 'Giphy search returned zero giphys';
      return (
        <InfiniteScroll
          batchSize={GIPHY_BATCH_SIZE}
        >
          {giphys.map((giphy, index) => (
            <GiphyCard
              key={giphy.id}
              giphy={giphy}
              index={index}
              clickGiphy={this.clickFavorite}
            />
          ))}
        </InfiniteScroll>
      );
    }

    render() {
      const { giphys, isFetching, search } = this.props;
      return (
        <React.Fragment>
          <GiphySearch
            fetchGiphys={this.fetchGiphys}
            search={search}
          />
          {!giphys || isFetching ? GiphySearchPageContainer.renderLoading() : this.renderGiphys()}
        </React.Fragment>
      );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GiphySearchPageContainer);
