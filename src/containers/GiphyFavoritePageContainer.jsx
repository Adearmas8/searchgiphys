import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/core/styles';

import { fetchGiphysByIDAction } from '../actions/apiActions';
import { getFavoriteGiphys } from '../reducers/mainReducer';
import { getFavorites } from '../selectors/selectors';
import GiphyCard from '../components/GiphyCard';

const styles = theme => ({
  pageContainer: {
    flex: '1 1 auto',
    display: 'flex',
  },
  listContainer: {
    flex: '30%',
    overflowX: 'auto',
    marginTop: '5px',
  },
  iframeContainer: {
    flex: '70%',
  },
  iframe: {
    height: '100%', width: '100%',
  },
});

const mapStateToProps = state => ({
  favoriteIDs: getFavorites(state),
  favoriteGiphys: getFavoriteGiphys(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchFavorites: fetchGiphysByIDAction,
}, dispatch);

class GiphyFavoritePageContainer extends Component {
  state = { selectedFavoriteID: null };

  componentWillMount() {
    const { fetchFavorites, favoriteIDs } = this.props;
    fetchFavorites(favoriteIDs.join(','));
  }

    selectGiphy = (giphy) => {
      this.setState({ selectedFavoriteID: giphy.id });
    };

    renderGiphys() {
      const { favoriteGiphys } = this.props;
      const { selectedFavoriteID } = this.state;
      if (!favoriteGiphys) return <CircularProgress />;
      return favoriteGiphys.map((giphy, index) => (
        <GiphyCard
          key={giphy.id}
          isFavorite={false}
          isSelected={(!selectedFavoriteID && !index) || selectedFavoriteID === giphy.id}
          clickGiphy={this.selectGiphy}
          giphy={giphy}
          index={index}
        />
      ));
    }

    renderIframe() {
      const { favoriteGiphys, classes } = this.props;
      const { selectedFavoriteID } = this.state;
      if (!favoriteGiphys) return null;
      const giphy = selectedFavoriteID ? favoriteGiphys.find(g => g.id === selectedFavoriteID)
        : favoriteGiphys[0];
      return (
        <iframe title="Selected Giphy" className={classes.iframe} src={giphy.embed} />
      );
    }

    render() {
      const { favoriteGiphys, classes } = this.props;
      if (favoriteGiphys && !favoriteGiphys.length) {
        return <p>No favorites added yet, go <Link to="/">here</Link> to add some</p>;
      }
      return (
        <div className={classes.pageContainer}>
          <div className={classes.listContainer}>
            {this.renderGiphys()}
          </div>
          <div className={classes.iframeContainer}>
            {this.renderIframe()}
          </div>
        </div>

      );
    }
}

const StyledContainer = withStyles(styles)(GiphyFavoritePageContainer);
export default connect(mapStateToProps, mapDispatchToProps)(StyledContainer);
