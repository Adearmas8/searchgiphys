import { createSelector } from 'reselect';

export const getFavorites = state => state.favorites;

export const getFavoritesSet = createSelector(
  getFavorites,
  favorites => favorites.reduce((set, f) => {
    set.add(f);
    return set;
  }, new Set()),
);

export const getFavoritesLength = createSelector(
  getFavorites,
  favorites => favorites.reduce(number => ++number, 0),
);
