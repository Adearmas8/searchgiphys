import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import mainReducer from './reducers/mainReducer';

function getFavorites() {
  const favoriteString = localStorage.getItem('favoriteGiphys');
  const favorites = favoriteString ? JSON.parse(favoriteString) : [];
  return {
    favorites,
  };
}

export default function configureStore(initialState = {}) {
  return createStore(
    mainReducer,
    getFavorites(),
    applyMiddleware(thunk),
  );
}
