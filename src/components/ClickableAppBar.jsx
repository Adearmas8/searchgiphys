import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import Badge from '@material-ui/core/Badge';
import { withStyles } from '@material-ui/core/styles';
import FavoriteIcon from '@material-ui/icons/Favorite';
import SearchIcon from '@material-ui/icons/Search';
import { withRouter } from 'react-router-dom';

const styles = theme => ({
  root: {
    width: '100%',
    flex: '0 1 auto',
  },
  grow: {
    flexGrow: 1,
  },
});

class ClickableAppBar extends React.Component {
    handleChangePage = () => {
      const { history, location } = this.props;
      history.push(location.pathname === '/' ? '/favorites' : '/');
    };

    renderIcon() {
      const { location, numberOfFavorites } = this.props;
      if (location.pathname === '/') {
        return (
          <Tooltip title="Go to favorites">
            <IconButton
              color="inherit"
              onClick={this.handleChangePage}
            >
              <Badge badgeContent={numberOfFavorites} color="secondary">
                <FavoriteIcon />
              </Badge>
            </IconButton>
          </Tooltip>
        );
      }
      return (
        <Tooltip title="Go to search">
          <IconButton
            color="inherit"
            onClick={this.handleChangePage}
          >
            <SearchIcon />
          </IconButton>
        </Tooltip>
      );
    }

    render() {
      const { classes, location } = this.props;
      return (
        <div className={classes.root}>
          <AppBar position="static">
            <Toolbar>
              <Typography variant="h6" color="inherit">
                { location.pathname === '/' ? 'GIPHY Search' : 'Favorite GIPHYs'}
              </Typography>
              <div className={classes.grow} />
              { this.renderIcon() }
            </Toolbar>
          </AppBar>
        </div>
      );
    }
}

ClickableAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

const Styled = withStyles(styles)(ClickableAppBar);
export default withRouter(Styled);
