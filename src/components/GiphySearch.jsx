import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import SearchIcon from '@material-ui/icons/Search';

const styles = theme => ({
  searchContainer: {
    flex: '0 1 auto',
  },
  searchInput: {
    marginLeft: 8,
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
});

class GiphySearch extends React.Component {
    state = { search: '', isChanged: false };

    handleSearchChange = (e) => {
      this.setState({ search: e.target.value, isChanged: true });
    };

    fetchGiphys = () => {
      const { fetchGiphys, search } = this.props;
      const { search: stateSearch, isChanged } = this.state;
      fetchGiphys(isChanged ? stateSearch : search, true);
    };

    keyPress = (e) => {
      if (e.charCode === 13 || e.keyCode === 13) this.fetchGiphys();
    };

    render() {
      const { classes, search } = this.props;
      const { search: stateSearch, isChanged } = this.state;
      return (
        <div className={classes.searchContainer}>
          <InputBase
            className={classes.searchInput}
            placeholder="Search Giphys"
            value={stateSearch || isChanged ? stateSearch : search}
            onChange={this.handleSearchChange}
            onKeyPress={this.keyPress}
          />
          <Tooltip title="Search">
            <IconButton onClick={this.fetchGiphys} className={classes.iconButton} aria-label="Search">
              <SearchIcon />
            </IconButton>
          </Tooltip>
        </div>
      );
    }
}

GiphySearch.propTypes = {
  fetchGiphys: PropTypes.func.isRequired,
};

export default withStyles(styles)(GiphySearch);
