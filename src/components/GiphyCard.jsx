import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FavoriteButton from './FavoriteButton';

const styles = theme => ({
  bigCard: {
    maxWidth: 250,
    display: 'inline-block',
    boxShadow: '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
    position: 'relative',
  },
  bigMedia: {
    height: 200,
    width: 250,
    display: 'block',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: '50% 0',
  },
  actionsContainer: {
    position: 'absolute',
    top: 0,
    height: 200,
    width: 250,
    backgroundColor: 'transparent',
  },
  smallCard: {
    maxWidth: 200,
    margin: '1px',
    display: 'inline-block',
    boxShadow: '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
    position: 'relative',
  },
  smallCardSelected: {
    maxWidth: 200,
    margin: '3px',
    display: 'inline-block',
    boxShadow: '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
    position: 'relative',
    outline: '3px solid rgb(63, 81, 181)',
  },
  smallMedia: {
    height: 200,
    width: 200,
    display: 'block',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: '50% 0',
  },
  actions: {
    position: 'absolute',
    top: 0,
    right: 0,
    padding: 5,
    backgroundColor: 'transparent',
    '&:hover': {
      backgroundColor: 'white',
    },
  },
});

const GiphyCard = ({
  giphy, classes, clickGiphy, index, isFavorite, isSelected,
}) => (
  <div onClick={() => clickGiphy(giphy, index)} className={isFavorite ? classes.bigCard : isSelected ? classes.smallCardSelected : classes.smallCard}>
    <img alt="" className={isFavorite ? classes.bigMedia : classes.smallMedia} src={giphy.url} />
    { isFavorite
      ? (
        <React.Fragment>
          <div className={classes.actionsContainer}>
            <div className={classes.actions}>
              <FavoriteButton
                giphy={giphy}
              />
            </div>
          </div>
        </React.Fragment>
      ) : null }
  </div>
);

GiphyCard.defaultProps = {
  isFavorite: true,
  isSelected: false,
};

GiphyCard.propTypes = {
  giphy: PropTypes.object.isRequired,
  clickGiphy: PropTypes.func.isRequired,
  isFavorite: PropTypes.bool,
  isSelected: PropTypes.bool,
};

export default withStyles(styles)(GiphyCard);
