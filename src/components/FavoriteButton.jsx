import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';

const styles = theme => ({
  root: {
    padding: 0,
    fontSize: 12,
  },
  colorPrimary: {
    color: 'gray',
  },
  colorSecondary: {
    color: 'red',
  },
});

const FavoriteButton = ({
  giphy, classes,
}) => (
  <IconButton
    className={classes.root}
    classes={{ colorPrimary: classes.colorPrimary, colorSecondary: classes.colorSecondary }}
    aria-label="Add to favorites"
    color={giphy.isFavorite ? 'secondary' : 'primary'}
  >
    <FavoriteIcon
      fontSize="inherit"
    />
  </IconButton>
);

FavoriteButton.propTypes = {
  giphy: PropTypes.object.isRequired,
};


export default withStyles(styles, { name: 'MuiIconButton' })(FavoriteButton);
