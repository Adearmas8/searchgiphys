import { getGiphys } from '../reducers/mainReducer';

export const ADD_FAVORITE = 'ADD_FAVORITE';
export const REMOVE_FAVORITE = 'REMOVE_FAVORITE';

export function addFavoriteAction(id, index) {
  return (dispatch, getState) => {
    const localString = localStorage.getItem('favoriteGiphys');
    const favoriteArray = localString ? JSON.parse(localString) : [];
    const giphys = [...getGiphys(getState())];
    giphys[index] = {
      ...giphys[index],
      isFavorite: true,
    };
    // using localstorage as a database for now
    favoriteArray.push(id);
    localStorage.setItem('favoriteGiphys', JSON.stringify(favoriteArray));
    return dispatch({
      type: ADD_FAVORITE,
      payload: { favorites: favoriteArray, giphys },
    });
  };
}

export function removeFavoriteAction(id, ind) {
  return (dispatch, getState) => {
    const localString = localStorage.getItem('favoriteGiphys');
    const favoriteArray = JSON.parse(localString).filter(currentID => currentID !== id);
    const giphys = getGiphys(getState()).map((g) => {
      if (g.id === id) return { ...g, isFavorite: false };
      return g;
    });
    localStorage.setItem('favoriteGiphys', JSON.stringify(favoriteArray));
    return dispatch({
      type: REMOVE_FAVORITE,
      payload: { favorites: favoriteArray, giphys },
    });
  };
}
