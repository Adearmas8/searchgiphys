import fetch from 'cross-fetch';
import { getFavoritesSet } from '../selectors/selectors';
import { getGiphys } from '../reducers/mainReducer';

export const FETCH_GIPHY = 'FETCH_GIPHY';
export const FETCH_FAVORITE_GIPHYS = 'FETCH_FAVORITE_GIPHYS';
export const FETCH_GIPHY_SUCCESS = 'FETCH_GIPHY_SUCCESS';
export const FETCH_GIPHY_ERROR = 'FETCH_GIPHY_ERROR';
export const FETCH_FAVORITE_GIPHY_SUCCESS = 'FETCH_FAVORITE_GIPHY_SUCCESS';
export const FETCH_FAVORITE_GIPHY_ERROR = 'FETCH_FAVORITE_GIPHY_ERROR';

const GIPHY_API_KEY = 'GZKGwdu6xlIM0iV58yFKJOFLqj0NLXFw';

export function fetchGiphyAction(search, total = 25, offset = 0) {
  return async (dispatch, getState) => {
    if (!offset) {
      dispatch({
        type: FETCH_GIPHY,
      });
    }
    try {
      const res = await fetch(`http://api.giphy.com/v1/gifs/search?api_key=${GIPHY_API_KEY}&limit=${total}&offset=${offset}&q=${search.replace(' ', '+')}`);
      const response = await res.json();
      if (res.status >= 400) {
        dispatch({
          type: FETCH_GIPHY_ERROR,
          payload: res.error,
        });
        throw new Error(res.error);
      }
      const responseGiphys = parseGiphys(response, getFavoritesSet(getState()));
      const stateGiphys = getGiphys(getState());
      dispatch({
        type: FETCH_GIPHY_SUCCESS,
        payload: {
          giphys: offset ? [...stateGiphys, ...responseGiphys] : responseGiphys, totalCount: response.pagination.total_count, offset: response.pagination.offset, search,
        },
      });
    } catch (err) {
      dispatch({
        type: FETCH_GIPHY_ERROR,
        payload: err,
      });
      console.error(err);
    }
  };
}

export function fetchGiphysByIDAction(ids) {
  if (!ids || !ids.length) {
    return {
      type: FETCH_FAVORITE_GIPHY_SUCCESS,
      payload: { favoriteGiphys: [] },
    };
  }
  return async (dispatch, getState) => {
    dispatch({
      type: FETCH_FAVORITE_GIPHYS,
    });
    try {
      const res = await fetch(`http://api.giphy.com/v1/gifs?api_key=${GIPHY_API_KEY}&ids=${ids}`);
      const response = await res.json();
      if (res.status >= 400) {
        dispatch({
          type: FETCH_FAVORITE_GIPHYS,
          payload: res.error,
        });
        throw new Error(res.error);
      }
      const responseGiphys = parseGiphys(response, getFavoritesSet(getState()));
      dispatch({
        type: FETCH_FAVORITE_GIPHY_SUCCESS,
        payload: {
          favoriteGiphys: responseGiphys,
        },
      });
    } catch (err) {
      dispatch({
        type: FETCH_FAVORITE_GIPHY_ERROR,
        payload: err,
      });
      console.error(err);
    }
  };
}

function parseGiphys(response, favoritesSet) {
  if (!response.data || !response.data.length) return [];
  return response.data.map(g => ({
    id: g.id, url: g.images.fixed_height.url, width: g.images.fixed_height.width, isFavorite: favoritesSet.has(g.id), embed: g.embed_url,
  }));
}
