import {
  FETCH_GIPHY_SUCCESS,
  FETCH_GIPHY,
  FETCH_FAVORITE_GIPHY_SUCCESS
} from '../actions/apiActions';

import {
  ADD_FAVORITE,
  REMOVE_FAVORITE,
} from '../actions/localStorageActions';

export default (state = {}, action) => {
  switch (action.type) {
    case FETCH_GIPHY: {
      return {
        ...state,
        isFetching: true,
      };
    }
    case FETCH_GIPHY_SUCCESS: {
      return {
        ...state,
        search: action.payload.search,
        giphys: action.payload.giphys,
        offset: action.payload.offset,
        totalCount: action.payload.totalCount,
        isFetching: false,
      };
    }
    case FETCH_FAVORITE_GIPHY_SUCCESS: {
      return {
        ...state,
        favoriteGiphys: action.payload.favoriteGiphys,
      };
    }
    case REMOVE_FAVORITE:
    case ADD_FAVORITE: {
      return {
        ...state,
        favorites: action.payload.favorites,
        giphys: action.payload.giphys,
      };
    }
    default:
      return state;
  }
};


export function getGiphys(state) {
  if (!state || !state.giphys) return null;
  return state.giphys;
}

export function getFavoriteGiphys(state) {
  if (!state || !state.favoriteGiphys) return null;
  return state.favoriteGiphys;
}

export function getIsFetching(state) {
  if (!state || !state.isFetching) return false;
  return true;
}

export function getTotalCount(state) {
  if (!state || !state.totalCount) return 0;
  return state.totalCount;
}

export function getOffset(state) {
  if (!state || !state.offset) return 0;
  return state.offset;
}

export function getSearch(state) {
  if (!state || !state.search) return '';
  return state.search;
}
