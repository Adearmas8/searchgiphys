import React from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { getFavoritesLength } from './selectors/selectors';
import './App.css';

import GiphySearchPageContainer from './containers/GiphySearchPageContainer';
import GiphyFavoritePageContainer from './containers/GiphyFavoritePageContainer';
import AppBar from './components/ClickableAppBar';

const mapStateToProps = state => ({
  numberOfFavorites: getFavoritesLength(state),
});

const App = ({ numberOfFavorites }) => (
  <Router>
    <div className="App">
      <AppBar
        numberOfFavorites={numberOfFavorites}
      />
      <Route exact path="/" component={GiphySearchPageContainer} />
      <Route exact path="/favorites" component={GiphyFavoritePageContainer} />
    </div>
  </Router>
);

export default connect(mapStateToProps, () => ({}))(App);
